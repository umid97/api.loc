$(function(){
    $('.login').change(function(){
        $.ajax({
            data: {login: $(this).val()},
            type: 'GET',
            url: 'http://localhost/shops.loc/category/is-login',
            success: function(res){
                if(res){
                    $('#users-login').val(' ');
                    $('#users-login').css({
                        border: '1px solid red'
                    });
                    $('.field-users-login label').html('Login mavjud!');
                }
                else{
                    $(this).css({
                        'background': 'grey'
                    });
                    $('.field-users-login label').html('Foydalanuvchi logini');
                    $('#users-login').css({
                        border: '1px solid #ccc'
                    });
                }
            },
            error: function(){
                alert('Xatolik mavjud!');
            }
        })
    })
})