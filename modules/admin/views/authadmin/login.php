<?php
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>
<div class="log-in pb-100">
    <div class="container">
        <div class="row">
            <div class="col-sm-4"></div>
            <!-- Returning Customer Start -->
            <div class="col-sm-4">
                <div class="well">
                    <div class="return-customer">
                        <p class="mb-10">Login va Parol orqali kabinet kirish</p>
                         <?php
                         $form = ActiveForm::begin(); ?>

                         <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                         <?= $form->field($model, 'password')->passwordInput() ?>



                        <div class="form-group">
                            <div class="col-lg-offset-1 col-lg-11">
                                 <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                            </div>
                        </div>

                         <?php ActiveForm::end();
                         ?>
                    </div>
                </div>
            </div>
            <!-- Returning Customer End -->
        </div>
        <!-- Row End -->
    </div>
    <!-- Container End -->
</div>
<!-- LogIn Page End -->

