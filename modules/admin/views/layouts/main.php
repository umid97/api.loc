<?php
use yii\helpers\Html;
use yii\helpers\Url;
use app\assets\AppAsset;
AppAsset::register($this);
$this->title = 'Xush kelibsiz!';
?>
<?php $this->beginPage() ?>
<!doctype html>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
     <meta charset="<?= Yii::$app->charset ?>">
     <meta http-equiv="X-UA-Compatible" content="IE=edge">
     <meta name="viewport" content="width=device-width, initial-scale=1">
     <?php $this->registerCsrfMetaTags() ?>
     <title><?= Html::encode($this->title) ?></title>
     <?php $this->head() ?>
     <link rel="shortcut icon" type="image/x-icon" href="<?=Url::base()?>/img/icon/favicon.png">

     <!-- modernizr js -->
     <script src="<?=Url::base()?>/js/vendor/modernizr-2.8.3.min.js"></script>
</head>

<body>

<?php $this->beginBody() ?>
<?php
echo $content;
?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
