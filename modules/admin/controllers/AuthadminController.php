<?php

namespace app\modules\admin\controllers;
use Yii;
use app\models\Admin;
use app\models\LoginForm;

class AuthadminController extends AppAdminController
{
     public function actionLogin()
     {
          if (!Yii::$app->user->isGuest) {
               return $this->goHome();
          }

          $model = new LoginForm();
          if ($model->load(Yii::$app->request->post()) && $model->login()) {
               return $this->goBack();
          }

          $model->password = '';
          return $this->render('login', [
               'model' => $model,
          ]);
     }

     public function actionLogout()
     {
          Yii::$app->user->logout();

          return $this->goHome();
     }

     public function actionAuth(){
          $admin = Admin::findOne(1);
          Yii::$app->user->login($admin);
          if(Yii::$app->user->isGuest){
               return $this->redirect(['default/index']);
          }
          else{
               return $this->redirect(['authadmin/login']);
          }
     }

}