<?php

namespace app\modules\users\controllers;
use Yii;
use app\models\Users;


class AuthController extends AppUsersController
{
     public function actionLogin()
     {
          if (!Yii::$app->user->isGuest) {
               return $this->goHome();
          }

          $model = new LoginForm();
          if ($model->load(Yii::$app->request->post()) && $model->login()) {
               return $this->goBack();
          }

          $model->password = '';
          return $this->render('login', [
               'model' => $model,
          ]);
     }

     /**
      * Logout action.
      *
      * @return Response
      */
     public function actionLogout()
     {
          Yii::$app->user->logout();

          return $this->goHome();
     }

     public function actionTest(){
          $users = Users::findOne(1);
          Yii::$app->user->login($users);
          if(Yii::$app->user->isGuest){
               echo 'Mehmon';
          }
          else{
               echo 'Mehmon emas!';
          }
     }
}