<?php

namespace app\modules\users\controllers;

use yii\web\Controller;
use app\models\LoginForm;
use app\models\Users;
use Yii;

class DefaultController extends AppUsersController
{

     public function actionIndex()
    {
        return $this->render('index');
    }
}
