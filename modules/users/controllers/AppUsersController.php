<?php

namespace app\modules\users\controllers;
use yii\base\Controller;
use yii\filters\AccessControl;

class AppUsersController extends Controller
{
     public function behaviors()
     {
          return [
               'access' => [
                    'class' => \yii\filters\AccessControl::className(),
                    'rules' => [
                         [
                              'allow' => true,
                              'roles' => ['@'],
                         ],
                    ],
               ],
          ];
     }
}