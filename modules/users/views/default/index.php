<?php
    use yii\helpers\Url;
?>
<!-- Page Breadcrumb Start -->
<div class="main-breadcrumb mb-100" style="background: rgba(0, 0, 0, 0) url(<?=Url::base()?>/img/blog/5.png) no-repeat scroll center center / cover;">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="breadcrumb-content text-center ptb-70">
                    <ul class="breadcrumb-list breadcrumb">
                        <li><a href="<?=Url::home(); ?>">Bosh sahifa</a></li>
                        <li><a href="">Sizning ro'yxatingiz </a></li>
                    </ul>
                </div>
            </div>
        </div>
        <!-- Row End -->
    </div>
    <!-- Container End -->
</div>
<!-- Page Breadcrumb End -->
<!-- Wish List Start -->
<div class="cart-main-area wish-list pb-50">
    <div class="container">
        <!-- Section Title Start -->
        <div class="section-title mb-50">
            <h2>wish list</h2>
        </div>
        <!-- Section Title Start End -->
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <!-- Form Start -->
                <form action="#">
                    <!-- Table Content Start -->
                    <div class="table-content table-responsive">
                        <table>
                            <thead>
                            <tr>
                                <th class="product-remove">Remove</th>
                                <th class="product-thumbnail">Image</th>
                                <th class="product-name">Product</th>
                                <th class="product-price">Unit Price</th>
                                <th class="product-quantity">Stock Status</th>
                                <th class="product-subtotal">add to cart</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="product-remove"> <a href="#"><i class="fa fa-times" aria-hidden="true"></i></a></td>
                                <td class="product-thumbnail">
                                    <a href="#"><img src="<?=Url::base()?>/img/new-products/4_1.jpg" alt="cart-image" /></a>
                                </td>
                                <td class="product-name"><a href="#">Vestibulum suscipit</a></td>
                                <td class="product-price"><span class="amount">£165.00</span></td>
                                <td class="product-stock-status"><span>in stock</span></td>
                                <td class="product-add-to-cart"><a href="#">add to cart</a></td>
                            </tr>
                            <tr>
                                <td class="product-remove"> <a href="#"><i class="fa fa-times" aria-hidden="true"></i></a></td>
                                <td class="product-thumbnail">
                                    <a href="#"><img src="<?=Url::base()?>/img/new-products/5_1.jpg" alt="cart-image" /></a>
                                </td>
                                <td class="product-name"><a href="#">Vestibulum dictum magna</a></td>
                                <td class="product-price"><span class="amount">£50.00</span></td>
                                <td class="product-stock-status"><span>in stock</span></td>
                                <td class="product-add-to-cart"><a href="#">add to cart</a></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- Table Content Start -->
                </form>
                <!-- Form End -->
            </div>
        </div>
        <!-- Row End -->
    </div>
</div>
<!-- Wish List End -->