<?php

namespace app\controllers;
use app\models\Category;
use app\models\Products;
use yii\data\Pagination;
use app\models\Users;
use Yii;

class CategoryController extends AppController
{
     public function actionIndex(){
          return $this->render('index');
     }

     public function actionView(){
          $id = Yii::$app->request->get('id');
          $db = Category::findOne([
               'id' => $id
          ]);
          if(empty($db))
               throw new \yii\web\HttpException(404, 'Bunday malumotlar mavjud emas!');
          $query = Products::find()
               ->where(['category_id' => $id]);
          $page = new Pagination([
             'totalCount' => $query->count(),
             'pageSize' => 9,
             'forcePageParam' => false,
             'pageSizeParam' => false
          ]);
          $products = $query->offset($page->offset)
               ->limit($page->limit)
               ->all();
          return $this->render('view', compact('db', 'products', 'page'));
     }

     public function actionContact(){
          $users = new Users;
          if($users->load(Yii::$app->request->post())){
               $u = Users::find()
                    ->where(['login' => $users->login])
                    ->all();
               if(!empty($u)){
                    return $this->refresh();
               }
               else{
                    if($users->save()){
                         throw new \yii\web\HttpException(404, 'Royxatdan otib oldingiz! Login va Parol orqali kabinetingizga kirish imkoni mavjud! Login va parolni unutmang!');
                    }
                    else{
                         throw new \yii\web\HttpException(404, 'Malumotlar saqlanmadi!');
                    }

               }
          }
          else{
               return $this->render('contact', compact('users'));
          }
          return $this->render('contact', compact('users'));
     }

     public function actionIsLogin(){
          $login = Yii::$app->request->get('login');
          $users = Users::findOne(['login' => $login]);
          if(!empty($users)) return true;
          else return false;
     }

     public function actionLogin(){
          $model = new Users;
          if($model->load(Yii::$app->request->post())){
               $model->isAuth($model->username, $model->password);
          }
          return $this->render('login', compact('model'));
     }
}