<?php
     namespace app\components;
     use yii\base\Widget;
     use app\models\Category;
     use app\models\Products;
     use yii\helpers\Url;
     class Submenu extends Widget{
          public $name;
          public $sub;
          public $str;

          public function init()
          {
               parent::init();
          }

          public function getDbData(){
               $db = Category::find()
                    ->where(['status' => 'active'])
                    ->asArray()
                    ->all();
               $this->name = $db;
          }

          public function MenuWidgets(){
               $this->str = '<ul class="ht-dropdown mega-menu-2" style="">';
               foreach($this->name as $row){
                    $this->str .= '<li style=""><ul><li><a href="'.Url::to(["category/view", 'id'=>$row['id']]).'"><h3 style="text-align: center; text-align-last: center; font-size: 13px !important">'.$row['name'].'</h3></a></li></ul></li>';

               }
               $this->str .= '</ul>';
          }

          public function run()
          {
               $this->getDbData();
               $this->MenuWidgets();
               return $this->str;
          }


     }