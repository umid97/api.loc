<?php
     use yii\helpers\Url;
     use yii\widgets\LinkPager;
?>

<!-- Page Breadcrumb Start -->
<div class="main-breadcrumb mb-100" style="background-image: url('<?=Url::base()?>/img/blog/5.png')">
     <div class="container">
          <div class="row">
               <div class="col-sm-12">
                    <div class="breadcrumb-content text-center ptb-70">
                         <h1><?=$db->name; ?></h1>
                         <ul class="breadcrumb-list breadcrumb">
                              <li><a href="<?=Url::home()?>">Bosh sahifa</a></li>
                              <li><a href=""><?=$db->name; ?></a></li>
                         </ul>
                    </div>
               </div>
          </div>
     </div>
</div>
<!-- Page Breadcrumb End -->
<!-- Categories Product Start -->
<div class="all-categories pb-100">
     <div class="container">
          <div class="row">
               <!-- Sidebar Content Start -->

               <div class="col-md-9 col-md-push-3">
                    <!-- Best Seller Product Start -->
                    <div class="best-seller">
                         <div class="row mt-20">
                              <div class="col-md-3 col-sm-4 pull-left">
                                   <div class="grid-list-view">
                                        <ul class="list-inline">
                                             <li class="active"><a data-toggle="tab" href="#grid-view" aria-expanded="true"><i class="zmdi zmdi-view-dashboard"></i><i class="pe-7s-keypad"></i></a></li>
                                             <li><a data-toggle="tab" href="#list-view" aria-expanded="false"><i class="zmdi zmdi-view-list"></i><i class="pe-7s-menu"></i></a></li>
                                        </ul>
                                   </div>
                              </div>
                         </div>
                         <div class="row">
                              <div class="col-sm-12">
                                   <div class="tab-content categorie-list ">
                                        <div id="list-view" class="tab-pane fade">
                                            <?php if(!empty($products)): ?>
                                                <div class="row">
                                                  <!-- Main Single Product Start -->
                                                 <?php foreach($products as $key => $row): ?>
                                                  <div class="main-single-product fix">
                                                       <div class="col-sm-4">
                                                            <!-- Single Product Start -->
                                                            <div class="single-product">
                                                                 <!-- Product Image Start -->
                                                                 <div class="pro-img">
                                                                      <a href="product-page.html">
                                                                           <img class="primary-img" src="<?=Url::base()?>/img/new-products/2_1.jpg" alt="single-product">
                                                                      </a>
                                                                      <div class="quick-view">
                                                                           <a href="#" data-toggle="modal" data-target="#myModal"><i class="pe-7s-look"></i>quick view</a>
                                                                      </div>
                                                                      <?php if($row['new']): ?>
                                                                          <span class="sticker-new">new</span>
                                                                           <?php if($row['discount_price']): ?>
                                                                              <span style="position: absolute; top: 10px; left: 0; background: #5be7c4; width: 46px; text-transform: uppercase; font-weight: 700;
    font-size: 10px;
    text-align: center;
    line-height: 20px; height: 20px; color: white; "><?=$row['discount_price']?></span>
                                                                           <?php endif; ?>
                                                                      <?php else: ?>

                                                                      <?php endif; ?>
                                                                 </div>
                                                                 <!-- Product Image End -->
                                                            </div>
                                                            <!-- Single Product End -->
                                                       </div>
                                                       <div class="col-sm-8">
                                                            <!-- Product Content Start -->
                                                            <div class="thubnail-desc fix">
                                                                 <h4 class="product-header"><a href="product-page.html"><?=$row['name'];?></a></h4>
                                                                 <!-- Product Price Start -->
                                                                 <div class="pro-price mb-15">
                                                                      <ul class="pro-price-list">
                                                                           <li class="price">$<?=$row['price']; ?></li>
                                                                           <li class="mtb-50">
                                                                                <p>
                                                                                     <?=substr($row['content'], 0, 150); ?>
                                                                                </p>
                                                                           </li>
                                                                      </ul>
                                                                 </div>
                                                                 <!-- Product Price End -->
                                                                 <!-- Product Button Actions Start -->
                                                                 <div class="product-button-actions">
                                                                      <button class="add-to-cart" data-toggle="tooltip" title="Add to Cart">add to cart</button>
                                                                      <a href="wish-list.html" data-toggle="tooltip" title="Add to Wishlist" class="same-btn mr-15"><i class="pe-7s-like"></i></a>
                                                                      <button data-toggle="tooltip" title="Compare this Product" class="same-btn"><i class="pe-7s-repeat"></i></button>
                                                                 </div>
                                                                 <!-- Product Button Actions End -->
                                                            </div>
                                                            <!-- Product Content End -->
                                                       </div>
                                                  </div>
                                                  <?php endforeach; ?>
                                                  <!-- Main Single Product Start -->
                                             </div>
                                            <?php else: ?>
                                                <div class="alert alert-warning alert-dismissible" role="alert">
                                                    <strong>
                                                        Bunday mahsulot yo'q!
                                                    </strong>
                                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                            <?php endif; ?>
                                             <!-- Row End -->
                                             <div class="row mt-40 mb-70">
                                                  <div class="col-sm-6">
                                                       <?php
                                                            echo LinkPager::widget(['pagination' => $page]);
                                                       ?>
                                                       <!-- End of .blog-pagination -->
                                                  </div>

                                             </div>
                                             <!-- Row End -->
                                        </div>
                                        <!-- #list-view End -->
                                        <div id="grid-view" class="tab-pane fade in active mt-40">
                                             <?php if(!empty($products)): ?>
                                             <div class="row">
                                                  <?php foreach ($products as $row): ?>
                                                  <div class="col-md-4 col-sm-6">
                                                       <!-- Single Product Start -->
                                                       <div class="single-product">
                                                            <!-- Product Image Start -->
                                                            <div class="pro-img">
                                                                 <a href="product-page.html">
                                                                      <img class="primary-img" src="<?=Url::base()?>/img/new-products/1_1.jpg" alt="single-product">
                                                                      <img class="secondary-img" src="<?=Url::base()?>/img/new-products/1_2.jpg" alt="single-product">
                                                                 </a>
                                                                 <div class="quick-view">
                                                                      <a href="#" data-toggle="modal" data-target="#myModal"><i class="pe-7s-look"></i>quick view</a>
                                                                 </div>
                                                                 <?php if($row['new']): ?>
                                                                     <span class="sticker-new">new</span>
                                                                     <?php if($row['discount_price']): ?>
                                                                         <span style="position: absolute; top: 10px; left: 0; background: #5be7c4; width: 46px; text-transform: uppercase; font-weight: 700;
    font-size: 10px;
    text-align: center;
    line-height: 20px; height: 20px; color: white; "><?=$row['discount_price']?></span>
                                                                     <?php endif; ?>
                                                                 <?php else: ?>

                                                                 <?php endif; ?>
                                                            </div>
                                                            <!-- Product Image End -->
                                                            <!-- Product Content Start -->
                                                            <div class="pro-content text-center">
                                                                 <h4><a href="product-page.html"><?=$row['name']?></a></h4>
                                                                 <p class="price"><span>$<?=$row['price']?></span></p>
                                                                 <div class="action-links2">
                                                                      <a data-toggle="tooltip" title="Add to Cart" href="cart.html">add to cart</a>
                                                                 </div>
                                                            </div>
                                                            <!-- Product Content End -->
                                                       </div>
                                                       <!-- Single Product End -->
                                                  </div>
                                                  <?php endforeach; ?>
                                             </div>
                                             <?php else: ?>
                                                 <div class="alert alert-danger alert-dismissible" role="alert">
                                                         Bunday mahsulot yo'q!
                                                     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                         <span aria-hidden="true">&times;</span>
                                                     </button>
                                                 </div>
                                             <?php endif; ?>
                                             <!-- Row End -->
                                             <div class="row mt-40 mb-70">
                                                  <div class="col-sm-6">
                                                       <?php
                                                       echo LinkPager::widget(['pagination' => $page]);
                                                       ?>
                                                       <!-- End of .blog-pagination -->
                                                  </div>

                                             </div>
                                             <!-- Row End -->
                                        </div>
                                        <!-- #Grid-view End -->
                                   </div>
                                   <!-- .Tab Content End -->
                              </div>
                         </div>
                         <!-- Row End -->
                    </div>
                    <!-- Best Seller Product End -->
               </div>



               <!-- Sidebar Content End -->
               <!-- Sidebar Start -->
               <div class="col-md-3 col-md-pull-9">
                    <aside class="categorie-sidebar mb-100">

                         <!-- Filter Option Start -->
                         <div class="flter-option mb-80">
                              <h3>PRICE FILTER</h3>
                              <form action="#">
                                   <div id="slider-range"></div>
                                   <input type="text" id="amount" class="amount" readonly>
                              </form>
                         </div>
                         <!-- Filter Option End -->
                         <!-- Categories Color Start -->
                         <div class="cat-color mb-80">
                              <h3>Color</h3>
                              <ul class="cat-color-list">
                                   <li><a href="#">Black (13)</a></li>
                                   <li><a href="#">Blue (13)</a></li>
                                   <li><a href="#">Grey (13)</a></li>
                                   <li><a href="#">Green (13)</a></li>
                                   <li><a href="#">Red (13)</a></li>
                              </ul>
                         </div>
                         <!-- Categories Color End -->
                         <!-- Manufactures List Start -->
                         <div class="manufactures mb-80">
                              <h3>MANUFACTURERS</h3>
                              <ul class="manufactures-list">
                                   <li><a href="#">Manufacturers 1 (14)</a></li>
                                   <li><a href="#">Manufacturers 2 (13)</a></li>
                                   <li><a href="#">Manufacturers 3 (13)</a></li>
                                   <li><a href="#">Manufacturers 4 (14)</a></li>
                                   <li><a href="#">Manufacturers 5 (13)</a></li>
                              </ul>
                         </div>
                         <!-- Manufactures List End -->
                         <!-- Most Viewed Product Start -->
                         <div class="most-viewed">
                              <h3>most viewed</h3>
                              <!-- Most Viewed Product Activation Start -->
                              <div class="most-viewed-product owl-carousel">
                                   <!-- Triple Product Start -->
                                   <div class="triple-product">
                                        <!-- Single Product Start -->
                                        <div class="single-product mb-25">
                                             <!-- Product Image Start -->
                                             <div class="pro-img">
                                                  <a href="product-page.html">
                                                       <img class="primary-img" src="<?=Url::base()?>/img/new-products/2_1.jpg" alt="single-product">
                                                       <img class="secondary-img" src="<?=Url::base()?>/img/new-products/2_2.jpg" alt="single-product">
                                                  </a>
                                             </div>
                                             <!-- Product Image End -->
                                             <!-- Product Content Start -->
                                             <div class="pro-content">
                                                  <h4><a href="product-page.html">Carte Postal Clock</a></h4>
                                                  <p class="price"><span>$122.00</span></p>
                                             </div>
                                             <!-- Product Content End -->
                                        </div>
                                        <!-- Single Product End -->
                                        <!-- Single Product Start -->
                                        <div class="single-product mb-25">
                                             <!-- Product Image Start -->
                                             <div class="pro-img">
                                                  <a href="product-page.html">
                                                       <img class="primary-img" src="<?=Url::base()?>/img/new-products/3_1.jpg" alt="single-product">
                                                       <img class="secondary-img" src="<?=Url::base()?>/img/new-products/3_2.jpg" alt="single-product">
                                                  </a>
                                             </div>
                                             <!-- Product Image End -->
                                             <!-- Product Content Start -->
                                             <div class="pro-content">
                                                  <h4><a href="product-page.html">congue sitamet</a></h4>
                                                  <p class="price"><span>$2000.00</span></p>
                                             </div>
                                             <!-- Product Content End -->
                                        </div>
                                        <!-- Single Product End -->
                                        <!-- Single Product Start -->
                                        <div class="single-product mb-25">
                                             <!-- Product Image Start -->
                                             <div class="pro-img">
                                                  <a href="product-page.html">
                                                       <img class="primary-img" src="<?=Url::base()?>/img/new-products/4_1.jpg" alt="single-product">
                                                       <img class="secondary-img" src="<?=Url::base()?>/img/new-products/2_2.jpg" alt="single-product">
                                                  </a>
                                             </div>
                                             <!-- Product Image End -->
                                             <!-- Product Content Start -->
                                             <div class="pro-content">
                                                  <h4><a href="product-page.html">dictum idrisus</a></h4>
                                                  <p class="price"><span>$602.00</span></p>
                                             </div>
                                             <!-- Product Content End -->
                                        </div>
                                        <!-- Single Product End -->
                                   </div>
                                   <!-- Triple Product End -->
                                   <!-- Triple Product Start -->
                                   <div class="triple-product">
                                        <!-- Single Product Start -->
                                        <div class="single-product mb-25">
                                             <!-- Product Image Start -->
                                             <div class="pro-img">
                                                  <a href="product-page.html">
                                                       <img class="primary-img" src="<?=Url::base()?>/img/new-products/4_1.jpg" alt="single-product">
                                                       <img class="secondary-img" src="<?=Url::base()?>/img/new-products/2_2.jpg" alt="single-product">
                                                  </a>
                                             </div>
                                             <!-- Product Image End -->
                                             <!-- Product Content Start -->
                                             <div class="pro-content">
                                                  <h4><a href="product-page.html">Carte Postal Clock</a></h4>
                                                  <p class="price"><span>$122.00</span></p>
                                             </div>
                                             <!-- Product Content End -->
                                        </div>
                                        <!-- Single Product End -->
                                        <!-- Single Product Start -->
                                        <div class="single-product mb-25">
                                             <!-- Product Image Start -->
                                             <div class="pro-img">
                                                  <a href="product-page.html">
                                                       <img class="primary-img" src="<?=Url::base()?>/img/new-products/2_1.jpg" alt="single-product">
                                                       <img class="secondary-img" src="<?=Url::base()?>/img/new-products/2_2.jpg" alt="single-product">
                                                  </a>
                                             </div>
                                             <!-- Product Image End -->
                                             <!-- Product Content Start -->
                                             <div class="pro-content">
                                                  <h4><a href="product-page.html">congue sitamet</a></h4>
                                                  <p class="price"><span>$2000.00</span></p>
                                             </div>
                                             <!-- Product Content End -->
                                        </div>
                                        <!-- Single Product End -->
                                        <!-- Single Product Start -->
                                        <div class="single-product mb-25">
                                             <!-- Product Image Start -->
                                             <div class="pro-img">
                                                  <a href="product-page.html">
                                                       <img class="primary-img" src="<?=Url::base()?>/img/new-products/3_1.jpg" alt="single-product">
                                                       <img class="secondary-img" src="<?=Url::base()?>/img/new-products/3_2.jpg" alt="single-product">
                                                  </a>
                                             </div>
                                             <!-- Product Image End -->
                                             <!-- Product Content Start -->
                                             <div class="pro-content">
                                                  <h4><a href="product-page.html">dictum idrisus</a></h4>
                                                  <p class="price"><span>$602.00</span></p>
                                             </div>
                                             <!-- Product Content End -->
                                        </div>
                                        <!-- Single Product End -->
                                   </div>
                                   <!-- Triple Product End -->
                                   <!-- Triple Product Start -->
                                   <div class="triple-product">
                                        <!-- Single Product Start -->
                                        <div class="single-product mb-25">
                                             <!-- Product Image Start -->
                                             <div class="pro-img">
                                                  <a href="product-page.html">
                                                       <img class="primary-img" src="<?=Url::base()?>/img/new-products/2_1.jpg" alt="single-product">
                                                       <img class="secondary-img" src="<?=Url::base()?>/img/new-products/2_2.jpg" alt="single-product">
                                                  </a>
                                             </div>
                                             <!-- Product Image End -->
                                             <!-- Product Content Start -->
                                             <div class="pro-content">
                                                  <h4><a href="product-page.html">Carte Postal Clock</a></h4>
                                                  <p class="price"><span>$122.00</span></p>
                                             </div>
                                             <!-- Product Content End -->
                                        </div>
                                        <!-- Single Product End -->
                                        <!-- Single Product Start -->
                                        <div class="single-product mb-25">
                                             <!-- Product Image Start -->
                                             <div class="pro-img">
                                                  <a href="product-page.html">
                                                       <img class="primary-img" src="<?=Url::base()?>/img/new-products/3_1.jpg" alt="single-product">
                                                       <img class="secondary-img" src="<?=Url::base()?>/img/new-products/3_2.jpg" alt="single-product">
                                                  </a>
                                             </div>
                                             <!-- Product Image End -->
                                             <!-- Product Content Start -->
                                             <div class="pro-content">
                                                  <h4><a href="product-page.html">congue sitamet</a></h4>
                                                  <p class="price"><span>$2000.00</span></p>
                                             </div>
                                             <!-- Product Content End -->
                                        </div>
                                        <!-- Single Product End -->
                                        <!-- Single Product Start -->
                                        <div class="single-product mb-25">
                                             <!-- Product Image Start -->
                                             <div class="pro-img">
                                                  <a href="product-page.html">
                                                       <img class="primary-img" src="<?=Url::base()?>/img/new-products/4_1.jpg" alt="single-product">
                                                       <img class="secondary-img" src="<?=Url::base()?>/img/new-products/2_2.jpg" alt="single-product">
                                                  </a>
                                             </div>
                                             <!-- Product Image End -->
                                             <!-- Product Content Start -->
                                             <div class="pro-content">
                                                  <h4><a href="#">dictum idrisus</a></h4>
                                                  <p class="price"><span>$602.00</span></p>
                                             </div>
                                             <!-- Product Content End -->
                                        </div>
                                        <!-- Single Product End -->
                                   </div>
                                   <!-- Triple Product End -->
                              </div>
                              <!-- Most Viewed Product Activation End -->
                         </div>
                         <!-- Most Viewed Product End -->
                    </aside>
               </div>
               <!-- Sidebar End -->
          </div>
          <!-- Row End -->
     </div>
     <!-- Container End -->
</div>
<!-- Categories Product End -->