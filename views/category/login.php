<?php
     use yii\helpers\Url;
     use yii\widgets\ActiveForm;
     use yii\helpers\Html;
?>

<!-- Page Breadcrumb Start -->
<div class="main-breadcrumb mb-100" style="background: rgba(0, 0, 0, 0) url(<?=Url::base()?>/img/blog/5.png) no-repeat scroll center center / cover;">
     <div class="container">
          <div class="row">
               <div class="col-sm-12">
                    <div class="breadcrumb-content text-center ptb-70">
                         <ul class="breadcrumb-list breadcrumb">
                              <li><a href="<?=Url::home(); ?>">Bosh sahifa</a></li>
                              <li><a href="<?=Url::to(['category/contact']);?>">Registratsiya</a></li>
                              <li><a href="">Kirish</a></li>
                         </ul>
                    </div>
               </div>
          </div>
          <!-- Row End -->
     </div>
     <!-- Container End -->
</div>
<!-- Page Breadcrumb End -->
<!-- LogIn Page Start -->
<div class="log-in pb-100">
     <div class="container">
          <div class="row">
               <!-- New Customer Start -->
               <div class="col-sm-6">
                    <div class="well">
                         <div class="new-customer">
                              <h3>NEW CUSTOMER</h3>
                              <p class="mtb-10"><strong>Register</strong></p>
                              <p>By creating an account you will be able to shop faster, be up to date on an order's status, and keep track of the orders you have previously made</p>
                              <a class="customer-btn" href="register.html">continue</a>
                         </div>
                    </div>
               </div>
               <!-- New Customer End -->
               <!-- Returning Customer Start -->
               <div class="col-sm-6">
                    <div class="well">
                         <div class="return-customer">
                              <p class="mb-10">Login va Parol orqali kabinet kirish</p>
                              <?php
                                    $form = ActiveForm::begin();
                                    echo $form->field($model, 'username');
                                    echo $form->field($model, 'password')->passwordInput();
                                    echo Html::submitButton('Tekshirish', ['class' => 'btn btn-success btn-sm']);
                                    ActiveForm::end();
                              ?>
                         </div>
                    </div>
               </div>
               <!-- Returning Customer End -->
          </div>
          <!-- Row End -->
     </div>
     <!-- Container End -->
</div>
<!-- LogIn Page End -->