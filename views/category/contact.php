<?php
     use yii\helpers\Url;
     use yii\bootstrap\ActiveForm;
     use yii\helpers\Html;
     ?>
<!-- Page Breadcrumb Start -->
<div class="main-breadcrumb" style="background: rgba(0, 0, 0, 0) url(<?=Url::base()?>/img/blog/5.png) no-repeat scroll center center / cover;">
     <div class="container">
          <div class="row">
               <div class="col-sm-12">
                    <div class="breadcrumb-content text-center ptb-70">
                         <ul class="breadcrumb-list breadcrumb">
                              <li><a href="#">home</a></li>
                              <li><a href="#">contact</a></li>
                         </ul>
                    </div>
               </div>
          </div>
          <!-- Row End -->
     </div>
     <!-- Container End -->
</div>
<!-- Page Breadcrumb End -->

<!-- Contact Email Area Start -->
<div class="contact-email-area ptb-100">
     <div class="container">
          <div class="row">
               <div class="col-xs-12">
                    <h3 class="mb-5">Contact Us</h3>
                    <?php
                        $form = ActiveForm::begin();
                            echo $form->field($users, 'fullname');
                            echo $form->field($users, 'email');
                            echo $form->field($users, 'phone');
                            echo $form->field($users, 'address');
                            echo $form->field($users, 'username')->textInput(['class' => 'form-control login']);
                            echo $form->field($users, 'password')->passwordInput();
                            echo Html::submitButton('Yuborish', ['class' => 'btn btn-success btn-sm']);
                        ActiveForm::end();
                    ?>
               </div>
          </div>
     </div>
</div>
<!-- Contact Email Area End -->