<?php
    use yii\helpers\Url;
?>
<!-- Slider Area Start -->
<div class="slider-area pb-100">
    <!-- Main Slider Area Start -->
    <div class="slider-wrapper theme-default">
        <!-- Slider Background  Image Start-->
        <div id="slider" class="nivoSlider">
            <img src="<?=Url::base()?>/img/slider/1.jpg" data-thumb="img/slider/1.jpg" alt="" title="#htmlcaption" />
            <img src="<?=Url::base()?>/img/slider/2.jpg" data-thumb="img/slider/2.jpg" alt="" title="#htmlcaption2" />
        </div>
        <!-- Slider Background  Image Start-->
        <!-- Slider htmlcaption Start-->
        <div id="htmlcaption" class="nivo-html-caption slider-caption">
            <!-- Slider Text Start -->
            <div class="slider-text">
                <h2 class="wow fadeInLeft" data-wow-delay="1s">EAMES LOUGE CHAIR<br>AND OTOMAN</h2>
                <p class="wow fadeInRight" data-wow-delay="1s">Own an icon of modern design</p>
                <a class="wow bounceInDown" data-wow-delay="0.8s" href="categorie-page.html">shop now</a>
            </div>
            <!-- Slider Text End -->
        </div>
        <!-- Slider htmlcaption End -->
        <!-- Slider htmlcaption Start -->
        <div id="htmlcaption2" class="nivo-html-caption slider-caption">
            <!-- Slider Text Start -->
            <div class="slider-text">
                <h2 class="wow zoomInUp" data-wow-delay="0.5s">IF YOUR PURCHASE<br>IS OVER $350</h2>
                <p class="wow zoomInUp" data-wow-delay="0.6s">We’ll give you a FREE delivery!</p>
                <a class="wow zoomInUp" data-wow-delay="1s" href="categorie-page.html">shop now</a>
            </div>
            <!-- Slider Text End -->
        </div>
        <!-- Slider htmlcaption End -->
    </div>
    <!-- Main Slider Area End -->
</div>
<!-- Slider Area End -->
<!-- New Products Selection Start -->
<div class="new-products-selection pb-80">
    <div class="container">
        <div class="row">
            <!-- Section Title Start -->
            <div class="col-xs-12">
                <div class="section-title text-center mb-40">
                    <span class="section-desc mb-15">Top new in this week</span>
                    <h3 class="section-info">new products</h3>
                </div>
            </div>
            <!-- Section Title End -->
        </div>
        <div class="row">
            <div class="col-sm-12">
                <!-- New Products Activation Start -->
                <div class="new-products owl-carousel">
                    <!-- Double Product Start -->
                    <div class="double-products">
                        <!-- Single Product Start -->
                        <div class="single-product mb-25">
                            <!-- Product Image Start -->
                            <div class="pro-img">
                                <a href="product-page.html">
                                    <img class="primary-img" src="<?=Url::base()?>/img/new-products/1_1.jpg" alt="single-product">
                                    <img class="secondary-img" src="<?=Url::base()?>/img/new-products/1_2.jpg" alt="single-product">
                                </a>
                                <div class="quick-view">
                                    <a href="#" data-toggle="modal" data-target="#myModal"><i class="pe-7s-look"></i>quick view</a>
                                </div>
                                <span class="sticker-new">new</span>
                            </div>
                            <!-- Product Image End -->
                            <!-- Product Content Start -->
                            <div class="pro-content text-center">
                                <h4><a href="product-page.html">Sheepskin Pillow2</a></h4>
                                <p class="price"><span>$241.99</span></p>
                                <div class="action-links2">
                                    <a data-toggle="tooltip" title="Add to Cart" href="cart.html">add to cart</a>
                                </div>
                            </div>
                            <!-- Product Content End -->
                        </div>
                        <!-- Single Product End -->
                        <!-- Single Product Start -->
                        <div class="single-product">
                            <!-- Product Image Start -->
                            <div class="pro-img">
                                <a href="product-page.html">
                                    <img class="primary-img" src="<?=Url::base()?>/img/new-products/1_2.jpg" alt="single-product">
                                    <img class="secondary-img" src="<?=Url::base()?>/img/new-products/5_1.jpg" alt="single-product">
                                </a>
                                <div class="quick-view">
                                    <a href="#" data-toggle="modal" data-target="#myModal"><i class="pe-7s-look"></i>quick view</a>
                                </div>
                                <span class="sticker-new">new</span>
                            </div>
                            <!-- Product Image End -->
                            <!-- Product Content Start -->
                            <div class="pro-content text-center">
                                <h4><a href="product-page.html">Modern Desk Lamp</a></h4>
                                <p class="price"><span>$122.00</span></p>
                                <div class="action-links2">
                                    <a data-toggle="tooltip" title="Add to Cart" href="cart.html">add to cart</a>
                                </div>
                            </div>
                            <!-- Product Content End -->
                        </div>
                        <!-- Single Product End -->
                    </div>
                    <!-- Double Product End -->
                    <!-- Double Product Start -->
                    <div class="double-products">
                        <!-- Single Product Start -->
                        <div class="single-product mb-25">
                            <!-- Product Image Start -->
                            <div class="pro-img">
                                <a href="product-page.html">
                                    <img class="primary-img" src="<?=Url::base()?>/img/new-products/2_1.jpg" alt="single-product">
                                    <img class="secondary-img" src="<?=Url::base()?>/img/new-products/2_2.jpg" alt="single-product">
                                </a>
                                <div class="quick-view">
                                    <a href="#" data-toggle="modal" data-target="#myModal"><i class="pe-7s-look"></i>quick view</a>
                                </div>
                                <span class="sticker-new">new</span>
                            </div>
                            <!-- Product Image End -->
                            <!-- Product Content Start -->
                            <div class="pro-content text-center">
                                <h4><a href="product-page.html">Carte Postal Clock</a></h4>
                                <p class="price"><span>$2000</span></p>
                                <div class="action-links2">
                                    <a data-toggle="tooltip" title="Add to Cart" href="cart.html">add to cart</a>
                                </div>
                            </div>
                            <!-- Product Content End -->
                        </div>
                        <!-- Single Product End -->
                        <!-- Single Product Start -->
                        <div class="single-product">
                            <!-- Product Image Start -->
                            <div class="pro-img">
                                <a href="product-page.html">
                                    <img class="primary-img" src="<?=Url::base()?>/img/new-products/6_1.jpg" alt="single-product">
                                    <img class="secondary-img" src="<?=Url::base()?>/img/new-products/6_2.jpg" alt="single-product">
                                </a>
                                <div class="quick-view">
                                    <a href="#" data-toggle="modal" data-target="#myModal"><i class="pe-7s-look"></i>quick view</a>
                                </div>
                                <span class="sticker-new">new</span>
                            </div>
                            <!-- Product Image End -->
                            <!-- Product Content Start -->
                            <div class="pro-content text-center">
                                <h4><a href="product-page.html">Nulla massa est</a></h4>
                                <p class="price"><span>$602</span></p>
                                <div class="action-links2">
                                    <a data-toggle="tooltip" title="Add to Cart" href="cart.html">add to cart</a>
                                </div>
                            </div>
                            <!-- Product Content End -->
                        </div>
                        <!-- Single Product End -->
                    </div>
                    <!-- Double Product End -->
                    <!-- Double Product Start -->
                    <div class="double-products">
                        <!-- Single Product Start -->
                        <div class="single-product mb-25">
                            <!-- Product Image Start -->
                            <div class="pro-img">
                                <a href="product-page.html">
                                    <img class="primary-img" src="<?=Url::base()?>/img/new-products/3_1.jpg" alt="single-product">
                                    <img class="secondary-img" src="<?=Url::base()?>/img/new-products/3_2.jpg" alt="single-product">
                                </a>
                                <div class="quick-view">
                                    <a href="#" data-toggle="modal" data-target="#myModal"><i class="pe-7s-look"></i>quick view</a>
                                </div>
                                <span class="sticker-new">new</span>
                            </div>
                            <!-- Product Image End -->
                            <!-- Product Content Start -->
                            <div class="pro-content text-center">
                                <h4><a href="product-page.html">congue sitamet</a></h4>
                                <p class="price"><span>$241.99</span></p>
                                <div class="action-links2">
                                    <a data-toggle="tooltip" title="Add to Cart" href="cart.html">add to cart</a>
                                </div>
                            </div>
                            <!-- Product Content End -->
                        </div>
                        <!-- Single Product End -->
                        <!-- Single Product Start -->
                        <div class="single-product">
                            <!-- Product Image Start -->
                            <div class="pro-img">
                                <a href="product-page.html">
                                    <img class="primary-img" src="<?=Url::base()?>/img/new-products/5_1.jpg" alt="single-product">
                                    <img class="secondary-img" src="<?=Url::base()?>/img/new-products/4_1.jpg" alt="single-product">
                                </a>
                                <div class="quick-view">
                                    <a href="#" data-toggle="modal" data-target="#myModal"><i class="pe-7s-look"></i>quick view</a>
                                </div>
                                <span class="sticker-new">new</span>
                            </div>
                            <!-- Product Image End -->
                            <!-- Product Content Start -->
                            <div class="pro-content text-center">
                                <h4><a href="product-page.html">Splacerat volutpat</a></h4>
                                <p class="price"><span>$122.99</span></p>
                                <div class="action-links2">
                                    <a data-toggle="tooltip" title="Add to Cart" href="cart.html">add to cart</a>
                                </div>
                            </div>
                            <!-- Product Content End -->
                        </div>
                        <!-- Single Product End -->
                    </div>
                    <!-- Double Product End -->
                    <!-- Double Product Start -->
                    <div class="double-products">
                        <!-- Single Product Start -->
                        <div class="single-product mb-25">
                            <!-- Product Image Start -->
                            <div class="pro-img">
                                <a href="product-page.html">
                                    <img class="primary-img" src="<?=Url::base()?>/img/new-products/4_1.jpg" alt="single-product">
                                    <img class="secondary-img" src="<?=Url::base()?>/img/new-products/2_2.jpg" alt="single-product">
                                </a>
                                <div class="quick-view">
                                    <a href="#" data-toggle="modal" data-target="#myModal"><i class="pe-7s-look"></i>quick view</a>
                                </div>
                                <span class="sticker-new">new</span>
                            </div>
                            <!-- Product Image End -->
                            <!-- Product Content Start -->
                            <div class="pro-content text-center">
                                <h4><a href="product-page.html">dictum idrisus</a></h4>
                                <p class="price"><span>$541.99</span></p>
                                <div class="action-links2">
                                    <a data-toggle="tooltip" title="Add to Cart" href="cart.html">add to cart</a>
                                </div>
                            </div>
                            <!-- Product Content End -->
                        </div>
                        <!-- Single Product End -->
                        <!-- Single Product Start -->
                        <div class="single-product">
                            <!-- Product Image Start -->
                            <div class="pro-img">
                                <a href="product-page.html">
                                    <img class="primary-img" src="<?=Url::base()?>/img/new-products/8_1.jpg" alt="single-product">
                                    <img class="secondary-img" src="<?=Url::base()?>/img/new-products/8_2.jpg" alt="single-product">
                                </a>
                                <div class="quick-view">
                                    <a href="#" data-toggle="modal" data-target="#myModal"><i class="pe-7s-look"></i>quick view</a>
                                </div>
                                <span class="sticker-sale">new</span>
                            </div>
                            <!-- Product Image End -->
                            <!-- Product Content Start -->
                            <div class="pro-content text-center">
                                <h4><a href="product-page.html">Maple Wood Table</a></h4>
                                <p class="price"><span>$255</span></p>
                                <div class="action-links2">
                                    <a data-toggle="tooltip" title="Add to Cart" href="cart.html">add to cart</a>
                                </div>
                            </div>
                            <!-- Product Content End -->
                        </div>
                        <!-- Single Product End -->
                    </div>
                    <!-- Double Product End -->
                    <!-- Double Product Start -->
                    <div class="double-products">
                        <!-- Single Product Start -->
                        <div class="single-product mb-25">
                            <!-- Product Image Start -->
                            <div class="pro-img">
                                <a href="product-page.html">
                                    <img class="primary-img" src="<?=Url::base()?>/img/new-products/2_2.jpg" alt="single-product">
                                    <img class="secondary-img" src="<?=Url::base()?>/img/new-products/2_1.jpg" alt="single-product">
                                </a>
                                <div class="quick-view">
                                    <a href="#" data-toggle="modal" data-target="#myModal"><i class="pe-7s-look"></i>quick view</a>
                                </div>
                                <span class="sticker-new">new</span>
                            </div>
                            <!-- Product Image End -->
                            <!-- Product Content Start -->
                            <div class="pro-content text-center">
                                <h4><a href="product-page.html">Decorative Vase</a></h4>
                                <p class="price"><span>$241.99</span></p>
                                <div class="action-links2">
                                    <a data-toggle="tooltip" title="Add to Cart" href="cart.html">add to cart</a>
                                </div>
                            </div>
                            <!-- Product Content End -->
                        </div>
                        <!-- Single Product End -->
                        <!-- Single Product Start -->
                        <div class="single-product">
                            <!-- Product Image Start -->
                            <div class="pro-img">
                                <a href="product-page.html">
                                    <img class="primary-img" src="<?=Url::base()?>/img/new-products/9_1.jpg" alt="single-product">
                                    <img class="secondary-img" src="<?=Url::base()?>/img/new-products/5_1.jpg" alt="single-product">
                                </a>
                                <div class="quick-view">
                                    <a href="#" data-toggle="modal" data-target="#myModal"><i class="pe-7s-look"></i>quick view</a>
                                </div>
                                <span class="sticker-new">new</span>
                            </div>
                            <!-- Product Image End -->
                            <!-- Product Content Start -->
                            <div class="pro-content text-center">
                                <h4><a href="product-page.html">Simplify One Drawer</a></h4>
                                <p class="price"><span>$117.99</span></p>
                                <div class="action-links2">
                                    <a data-toggle="tooltip" title="Add to Cart" href="cart.html">add to cart</a>
                                </div>
                            </div>
                            <!-- Product Content End -->
                        </div>
                        <!-- Single Product End -->
                    </div>
                    <!-- Double Product End -->
                </div>
                <!-- New Products Activation End -->
            </div>
        </div>
        <!-- Row End -->
    </div>
    <!-- Container End -->
</div>
<!-- New Products Selection End -->
<!-- New Products Banner Start -->
<div class="new-products-banner pb-100">
    <div class="container">
        <div class="row">
            <!-- Single Banner Start -->
            <div class="col-sm-4">
                <div class="single-banner zoom mb-30">
                    <img src="<?=Url::base()?>/img/products-banner/1.jpg" alt="product-banner">
                    <div class="banner-desc">
                        <a href="categorie-page.html">Decorative Accessories</a>
                        <p>25 products</p>
                    </div>
                </div>
            </div>
            <!-- Single Banner End -->
            <!-- Single Banner Start -->
            <div class="col-sm-8">
                <div class="single-banner zoom mb-30">
                    <img src="<?=Url::base()?>/img/products-banner/2.jpg" alt="product-banner">
                    <div class="banner-desc">
                        <a href="categorie-page.html">Chairs & Recliners</a>
                        <p>25 products</p>
                    </div>
                </div>
            </div>
            <!-- Single Banner End -->
            <!-- Single Banner Start -->
            <div class="col-sm-8">
                <div class="single-banner zoom">
                    <img src="<?=Url::base()?>/img/products-banner/3.jpg" alt="product-banner">
                    <div class="banner-desc">
                        <a href="categorie-page.html">Sofas & Loveseats</a>
                        <p>25 products</p>
                    </div>
                </div>
            </div>
            <!-- Single Banner End -->
            <!-- Single Banner Start -->
            <div class="col-sm-4">
                <div class="single-banner zoom">
                    <img src="<?=Url::base()?>/img/products-banner/4.jpg" alt="product-banner">
                    <div class="banner-desc">
                        <a href="categorie-page.html">Window Treatments</a>
                        <p>25 products</p>
                    </div>
                </div>
            </div>
            <!-- Single Banner End -->
        </div>
        <!-- Row End -->
    </div>
    <!-- Container End -->
</div>
<!-- New Products Banner End -->
<!-- Best Seller Products Start -->
<div class="best-seller-products pb-100">
    <div class="container">
        <div class="row">
            <!-- Section Title Start -->
            <div class="col-xs-12">
                <div class="section-title text-center mb-40">
                    <span class="section-desc mb-20">Top sale in this week</span>
                    <h3 class="section-info">best seller</h3>
                </div>
            </div>
            <!-- Section Title End -->
        </div>
        <!-- Row End -->
        <div class="row">
            <div class="col-sm-12">
                <!-- Best Seller Product Activation Start -->
                <div class="best-seller new-products owl-carousel">
                    <!-- Single Product Start -->
                    <div class="single-product">
                        <!-- Product Image Start -->
                        <div class="pro-img">
                            <a href="product-page.html">
                                <img class="primary-img" src="<?=Url::base()?>/img/new-products/1_2.jpg" alt="single-product">
                                <img class="secondary-img" src="<?=Url::base()?>/img/new-products/5_1.jpg" alt="single-product">
                            </a>
                            <div class="quick-view">
                                <a href="#" data-toggle="modal" data-target="#myModal"><i class="pe-7s-look"></i>quick view</a>
                            </div>
                            <span class="sticker-new">new</span>
                        </div>
                        <!-- Product Image End -->
                        <!-- Product Content Start -->
                        <div class="pro-content text-center">
                            <h4><a href="product-page.html">Decorative Vase</a></h4>
                            <p class="price"><span>$241.99</span></p>
                            <div class="action-links2">
                                <a data-toggle="tooltip" title="Add to Cart" href="cart.html">add to cart</a>
                            </div>
                        </div>
                        <!-- Product Content End -->
                    </div>
                    <!-- Single Product End -->
                    <!-- Single Product Start -->
                    <div class="single-product">
                        <!-- Product Image Start -->
                        <div class="pro-img">
                            <a href="product-page.html">
                                <img class="primary-img" src="<?=Url::base()?>/img/new-products/3_1.jpg" alt="single-product">
                                <img class="secondary-img" src="<?=Url::base()?>/img/new-products/6_2.jpg" alt="single-product">
                            </a>
                            <div class="quick-view">
                                <a href="#" data-toggle="modal" data-target="#myModal"><i class="pe-7s-look"></i>quick view</a>
                            </div>
                        </div>
                        <!-- Product Image End -->
                        <!-- Product Content Start -->
                        <div class="pro-content text-center">
                            <h4><a href="product-page.html">Decorative Vase</a></h4>
                            <p class="price"><span>$241.99</span></p>
                            <div class="action-links2">
                                <a data-toggle="tooltip" title="Add to Cart" href="cart.html">add to cart</a>
                            </div>
                        </div>
                        <!-- Product Content End -->
                    </div>
                    <!-- Single Product End -->
                    <!-- Single Product Start -->
                    <div class="single-product">
                        <!-- Product Image Start -->
                        <div class="pro-img">
                            <a href="product-page.html">
                                <img class="primary-img" src="<?=Url::base()?>/img/new-products/1_1.jpg" alt="single-product">
                                <img class="secondary-img" src="<?=Url::base()?>/img/new-products/2_2.jpg" alt="single-product">
                            </a>
                            <div class="quick-view">
                                <a href="#" data-toggle="modal" data-target="#myModal"><i class="pe-7s-look"></i>quick view</a>
                            </div>
                            <span class="sticker-new">new</span>
                        </div>
                        <!-- Product Image End -->
                        <!-- Product Content Start -->
                        <div class="pro-content text-center">
                            <h4><a href="product-page.html">Decorative Vase</a></h4>
                            <p class="price"><span>$241.99</span></p>
                            <div class="action-links2">
                                <a data-toggle="tooltip" title="Add to Cart" href="cart.html">add to cart</a>
                            </div>
                        </div>
                        <!-- Product Content End -->
                    </div>
                    <!-- Single Product End -->
                    <!-- Single Product Start -->
                    <div class="single-product">
                        <!-- Product Image Start -->
                        <div class="pro-img">
                            <a href="product-page.html">
                                <img class="primary-img" src="<?=Url::base()?>/img/new-products/6_1.jpg" alt="single-product">
                                <img class="secondary-img" src="<?=Url::base()?>/img/new-products/6_2.jpg" alt="single-product">
                            </a>
                            <div class="quick-view">
                                <a href="#" data-toggle="modal" data-target="#myModal"><i class="pe-7s-look"></i>quick view</a>
                            </div>
                            <span class="sticker-new">new</span>
                        </div>
                        <!-- Product Image End -->
                        <!-- Product Content Start -->
                        <div class="pro-content text-center">
                            <h4><a href="product-page.html">Decorative Vase</a></h4>
                            <p class="price"><span>$241.99</span></p>
                            <div class="action-links2">
                                <a data-toggle="tooltip" title="Add to Cart" href="cart.html">add to cart</a>
                            </div>
                        </div>
                        <!-- Product Content End -->
                    </div>
                    <!-- Single Product End -->
                    <!-- Single Product Start -->
                    <div class="single-product">
                        <!-- Product Image Start -->
                        <div class="pro-img">
                            <a href="product-page.html">
                                <img class="primary-img" src="<?=Url::base()?>/img/new-products/2_1.jpg" alt="single-product">
                                <img class="secondary-img" src="<?=Url::base()?>/img/new-products/2_2.jpg" alt="single-product">
                            </a>
                            <div class="quick-view">
                                <a href="#" data-toggle="modal" data-target="#myModal"><i class="pe-7s-look"></i>quick view</a>
                            </div>
                            <span class="sticker-new">new</span>
                        </div>
                        <!-- Product Image End -->
                        <!-- Product Content Start -->
                        <div class="pro-content text-center">
                            <h4><a href="product-page.html">Decorative Vase</a></h4>
                            <p class="price"><span>$241.99</span></p>
                            <div class="action-links2">
                                <a data-toggle="tooltip" title="Add to Cart" href="cart.html">add to cart</a>
                            </div>
                        </div>
                        <!-- Product Content End -->
                    </div>
                    <!-- Single Product End -->
                    <!-- Single Product Start -->
                    <div class="single-product">
                        <!-- Product Image Start -->
                        <div class="pro-img">
                            <a href="product-page.html">
                                <img class="primary-img" src="<?=Url::base()?>/img/new-products/8_1.jpg" alt="single-product">
                                <img class="secondary-img" src="<?=Url::base()?>/img/new-products/3_2.jpg" alt="single-product">
                            </a>
                            <div class="quick-view">
                                <a href="#" data-toggle="modal" data-target="#myModal"><i class="pe-7s-look"></i>quick view</a>
                            </div>
                            <span class="sticker-new">new</span>
                        </div>
                        <!-- Product Image End -->
                        <!-- Product Content Start -->
                        <div class="pro-content text-center">
                            <h4><a href="product-page.html">Decorative Vase</a></h4>
                            <p class="price"><span>$241.99</span></p>
                            <div class="action-links2">
                                <a data-toggle="tooltip" title="Add to Cart" href="cart.html">add to cart</a>
                            </div>
                        </div>
                        <!-- Product Content End -->
                    </div>
                    <!-- Single Product End -->
                </div>
                <!-- Best Seller Product Activation Start -->
                <div class="text-center shop-link-page mt-50"><a href="best-seller.html">Shop All Best Sellers</a></div>
            </div>
        </div>
        <!-- Row End -->
    </div>
    <!-- Container End -->
</div>
<!-- Best Seller Products End -->

<!-- Latest Blog Start -->
<div class="latest-blog off-white-bg ptb-100">
    <div class="container">
        <div class="row">
            <!-- Section Title Start -->
            <div class="col-xs-12">
                <div class="section-title text-center mb-40">
                    <span class="section-desc mb-20">Latest News From Our Blog</span>
                    <h3 class="section-info">LATEST BLOGS</h3>
                </div>
            </div>
            <!-- Section Title End -->
        </div>
        <!-- Row End -->
        <div class="row">
            <div class="col-sm-12">
                <!-- Blog Activation Start -->
                <div class="blog owl-carousel">
                    <!-- Single Blog Start -->
                    <div class="single-blog">
                        <div class="blog-img">
                            <a href="blog-details.html"><img src="<?=Url::base()?>/img/blog/1.jpg" alt="blog-image"></a>
                        </div>
                        <div class="blog-content">
                            <div class="blog-content-upper">
                                <h6 class="blog-title"><a href="blog-details.html">Amber Interiors</a></h6>
                                <p>Interior designer Amber Lewis’s blog takes you inside the creative workings of her Los Angeles–based studio.</p>
                            </div>
                            <div class="blog-content-lower">
                                <a href="https://themeforest.net/user/Nevara">Nevara</a>
                                <span class="f-right">05 Nov, 2017</span>
                            </div>
                        </div>
                    </div>
                    <!-- Single Blog End -->
                    <!-- Single Blog Start -->
                    <div class="single-blog">
                        <div class="blog-img">
                            <a href="blog-details.html"><img src="<?=Url::base()?>/img/blog/2.jpg" alt="blog-image"></a>
                        </div>
                        <div class="blog-content">
                            <div class="blog-content-upper">
                                <h6 class="blog-title"><a href="blog-details.html">Coco Lapine Design</a></h6>
                                <p>The blog of Belgian designer Sarah Van Peteghem, Coco Lapine Design is the space where she shares all.</p>
                            </div>
                            <div class="blog-content-lower">
                                <a href="https://themeforest.net/user/Nevara">Nevara</a>
                                <span class="f-right">04 Oct, 2017</span>
                            </div>
                        </div>
                    </div>
                    <!-- Single Blog End -->
                    <!-- Single Blog Start -->
                    <div class="single-blog">
                        <div class="blog-img">
                            <a href="blog-details.html"><img src="<?=Url::base()?>/img/blog/3.jpg" alt="blog-image"></a>
                        </div>
                        <div class="blog-content">
                            <div class="blog-content-upper">
                                <h6 class="blog-title"><a href="blog-details.html">Style by Emily Henderson</a></h6>
                                <p>The list wouldn’t be complete without the ever-stylish blog of interior design extraordinaire Emily Henderson.</p>
                            </div>
                            <div class="blog-content-lower">
                                <a href="https://themeforest.net/user/Nevara">Nevara</a>
                                <span class="f-right">16 Aug, 2017</span>
                            </div>
                        </div>
                    </div>
                    <!-- Single Blog End -->
                    <!-- Single Blog Start -->
                    <div class="single-blog">
                        <div class="blog-img">
                            <a href="blog-details.html"><img src="<?=Url::base()?>/img/blog/1.jpg" alt="blog-image"></a>
                        </div>
                        <div class="blog-content">
                            <div class="blog-content-upper">
                                <h6 class="blog-title"><a href="blog-details.html">Amber Interiors</a></h6>
                                <p>Interior designer Amber Lewis’s blog takes you inside the creative workings of her Los Angeles–based studio.</p>
                            </div>
                            <div class="blog-content-lower">
                                <a href="https://themeforest.net/user/Nevara">Nevara</a>
                                <span class="f-right">05 Nov, 2017</span>
                            </div>
                        </div>
                    </div>
                    <!-- Single Blog End -->
                    <!-- Single Blog Start -->
                    <div class="single-blog">
                        <div class="blog-img">
                            <a href="blog-details.html"><img src="<?=Url::base()?>/img/blog/2.jpg" alt="blog-image"></a>
                        </div>
                        <div class="blog-content">
                            <div class="blog-content-upper">
                                <h6 class="blog-title"><a href="blog-details.html">Coco Lapine Design</a></h6>
                                <p>Interior designer Amber Lewis’s blog takes you inside the creative workings of her Los Angeles–based studio.</p>
                            </div>
                            <div class="blog-content-lower">
                                <a href="https://themeforest.net/user/Nevara">Nevara</a>
                                <span class="f-right">04 Oct, 2017</span>
                            </div>
                        </div>
                    </div>
                    <!-- Single Blog End -->
                    <!-- Single Blog Start -->
                    <div class="single-blog">
                        <div class="blog-img">
                            <a href="blog-details.html"><img src="<?=Url::base()?>/img/blog/3.jpg" alt="blog-image"></a>
                        </div>
                        <div class="blog-content">
                            <div class="blog-content-upper">
                                <h6 class="blog-title"><a href="blog-details.html">Style by Emily Henderson</a></h6>
                                <p>The list wouldn’t be complete without the ever-stylish blog of interior design extraordinaire Emily Henderson.</p>
                            </div>
                            <div class="blog-content-lower">
                                <a href="https://themeforest.net/user/Nevara">Nevara</a>
                                <span class="f-right">05 Nov, 2017</span>
                            </div>
                        </div>
                    </div>
                    <!-- Single Blog End -->
                </div>
                <!-- Blog Activation End -->
            </div>
        </div>
        <!-- Row End -->
    </div>
    <!-- Container End -->
</div>
<!-- Latest Blog End -->