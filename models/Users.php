<?php

namespace app\models;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class Users extends ActiveRecord implements IdentityInterface
{
     public static function tableName(){
          return 'shop_users';
     }

     public function rules(){
          return [
            [['fullname', 'email', 'phone', 'address', 'username', 'password'], 'required'],
            [['fullname', 'username'], 'string'],
            ['email', 'email'],
            ['phone', 'integer'],
            ['address', 'string'],
            ['password', 'safe']
          ];
     }

     public function attributeLabels()
     {
          return [
            'fullname' => 'Fish',
            'email' => 'Elektron pochta',
            'phone' => 'Telefon nomer',
            'address' => 'Manzil',
            'username' => 'Foydalanuvchi logini',
            'password' => 'Foydalanuvchi paroli'
          ];
     }

     public static function findIdentity($id)
     {
          return static::findOne($id);
     }

     public static function findIdentityByAccessToken($token, $type = null)
     {

     }

     public static function findByUsername($login)
     {
          return static::findOne(['username' => $login]);
     }

     public function getId()
     {
          return $this->id;
     }

     public function getAuthKey()
     {
//          return $this->authKey;
     }

     public function validateAuthKey($authKey)
     {
//          return $this->authKey === $authKey;
     }

     public function validatePassword($password)
     {
          return \Yii::$app->security->validatePassword($password, $this->password);
     }

     public function isAuth($login, $parol){
          if($this->username === $login && $this->password === $parol){

          }
          else{

          }
     }


}