<?php

namespace app\models;
use yii\db\ActiveRecord;
use Yii;
use yii\web\IdentityInterface;

class Admin extends ActiveRecord implements IdentityInterface
{
     public static function tableName()
     {
          return 'shop_admin';
     }

     public function rules(){
          return [
            [['username', 'password'], 'required'],
            ['username', 'string']
          ];
     }

     public function attributeLabels()
     {
          return [
            'username' => 'Login',
            'password' => 'Parol',
          ];
     }

     public static function findIdentity($id)
     {
          return static::findOne($id);
     }

     public static function findIdentityByAccessToken($token, $type = null)
     {
//          return static::findOne(['access_token' => $token]);
     }

     public function getId()
     {
          return $this->id;
     }

     public function getAuthKey()
     {
//          return $this->auth_key;
     }

     public function validateAuthKey($authKey)
     {
//          return $this->getAuthKey() === $authKey;
     }

}