<?php

namespace app\models;
use yii\db\ActiveRecord;

class Category extends ActiveRecord
{
     public static function tableName()
     {
          return 'shop_category';
     }

     public function getCategory_sub(){
          return $this->hasMany(Products::className(), ['category_id' => 'id']);
     }
}