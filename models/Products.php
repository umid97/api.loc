<?php

namespace app\models;
use yii\db\ActiveRecord;

class Products extends ActiveRecord
{
     public static function tableName()
     {
          return 'shop_products';
     }

     public function getCategory(){
          return $this->hasOne(Category::className(), ['id' => 'category_id']);
     }
}